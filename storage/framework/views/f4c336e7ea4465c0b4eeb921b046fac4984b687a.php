<?php $__env->startSection('content'); ?>

<!-- content -->
<div class="card mb-3" style="width: 100%" >
  <div class="card-header">
    <i class="fas fa-table"></i>
    Top Up</div>
  <div class="card-body">
    <a class="btn btn-primary col-sm-12" style="margin-bottom: 10px;" href="/admin/topup/addGame"><i class="fa fa-fw fa-plus"></i><b> Add New Game</b></a>
    <a class="btn btn-success col-sm-12" style="margin-bottom: 10px;" href="/admin/topup/game"><i class="fa fa-fw fa-edit"></i><b> Edit Game</b></a>
    <a class="btn btn-primary col-sm-12" style="margin-bottom: 25px" href="/admin/topup/addPrice"><i class="fa fa-fw fa-plus"></i><b> Add New Price</b></a>
    <div class="table-responsive table-striped">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Image</th>
                <th>Games Name</th>
                <th>Balance</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Image</th>
                <th>Games Name</th>
                <th>Balance</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php $__currentLoopData = $topup->sortBy('id_game'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $topup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td width="155px"><img src="<?php echo e(asset('uploads/icon/'.$topup->image)); ?>" width="150px" height="200px"></td>
                <td><?php echo e($topup->game); ?></td>
                <td><?php echo e($topup->balance); ?> <?php echo e($topup->currency); ?></td>
                <td align="right">Rp. <?php echo e($topup->price); ?>,00</td>
                <td>
                    <span data-toggle="modal" data-target="#edit-modal-<?php echo e($topup->id_topup); ?>"><a href="#" data-placement="top" data-original-title="Edit topup" data-toggle="tooltip" class="btn btn-xs btn-success"><i class="fa fa-fw fa-edit"></i> <b class="hover-effect">Edit</b></a></span>
                    <?php echo $__env->make("admin.topup.editPrice", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <span data-toggle="modal" data-target="#delete-modal-<?php echo e($topup->id_topup); ?>" data-username="<?php echo e($topup->game); ?>" data-id="<?php echo e($topup->id_topup); ?>"><a href="#" data-placement="top" data-original-title="Delete topup" data-toggle="tooltip" class="btn btn-xs btn-danger"><i class="fa fa-fw fa-times"></i> <b class="hover-effect">Delete</b></a></span></td>
                    <?php echo $__env->make("admin.topup.deletePrice", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="card-footer small text-muted">Gamers Space</div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.navbar_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\GamersSpace\resources\views/admin/topup/view.blade.php ENDPATH**/ ?>