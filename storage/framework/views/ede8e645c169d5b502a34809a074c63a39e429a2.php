<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gamespace</title>
    <link rel="stylesheet" href="<?php echo e(URL::asset('template1/vendor/bootstrap/css/bootstrap.min.css')); ?>">

    <!-- Custom fonts for this template -->
    <link rel="stylesheet" href="<?php echo e(URL::asset('template1/vendor/fontawesome-free/css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(URL::asset('template1/vendor/simple-line-icons/css/simple-line-icons.css')); ?>">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet"
    type="text/css">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="<?php echo e(URL::asset('template1/css/landing-page.min.css')); ?>">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>

    <!-- Scripts -->
    <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/a6ca7083dc.js" crossorigin="anonymous"></script>
    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(URL::asset('css/layout.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(URL::asset('css/content.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(URL::asset('template2/assets/css/Features-Clean.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(URL::asset('template2/assets/css/PUSH---Bootstrap-Button-Pack.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(URL::asset('template2/assets/css/styles.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(URL::asset('template2/assets/bootstrap/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(URL::asset('template2/assets/css/-product-features.css')); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.1.1/aos.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Playfair+Display">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link rel="stylesheet" href="<?php echo e(URL::asset('template2/assets/fonts/font-awesome.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(URL::asset('template2/assets/css/Bold-BS4-Footer-Big-Logo.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(URL::asset('template2/assets/css/ebs-contact-form-1.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(URL::asset('template2/assets/css/Footer-with-social-media-icons.css')); ?>">
    <script src="https://kit.fontawesome.com/a6ca7083dc.js" crossorigin="anonymous"></script>
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md  shadow-sm" style="background:#0064D2">
            <div class="container">
                <a class="navbar-brand" href="<?php echo e(url('/')); ?>">

                </a>
                <a class="navbar-brand" href="/">
                    <img src="img/LOGO.png" alt="gamersspace">

                </a>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>
                    
                    <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <form class="form-inline my-2 my-lg-0">
                                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                            </form>
                            <!-- Authentication Links -->
                            <?php if(auth()->guard()->guest()): ?>
                            <li class="nav-item">
                                <a class="btn btn-warning" style="background:#F6C400; margin-bottom:0px " href="<?php echo e(route('login')); ?>"><?php echo e(__('Login')); ?></a>
                            </li>

                            <?php else: ?>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <?php echo e(Auth::user()->name); ?> <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        <?php echo e(__('Logout')); ?>

                                    </a>

                                    <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                        <?php echo csrf_field(); ?>
                                    </form>
                                </div>
                            </li>
                            <?php endif; ?>
                        </ul>
                </div>
            </div>
        </nav>


    


        <nav class="navbar navbar-expand-lg navbar-light " style="background:#0064D2">
            <div class="container">

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav nav-menu col-sm-12">
                        <li class="nav-item active">
                            <a class="nav-link" style=" font-family: Nunitosans " href="/">HOME <span class="sr-only">(current)</span></a>
                        </li>
                        <br>
                        <li class="nav-item active">
                            <a class="nav-link" style="font-family: Nunitosans" href="/events">EVENTS <span class="sr-only">(current)</span></a>
                        </li>
                        <br>
                        <li class="nav-item active">
                            <a class="nav-link" style="font-family: Nunitosans" href="/news">NEWS <span class="sr-only">(current)</span></a>
                        </li>
                        <br>
                        <br>
                        <li class="nav-item active">
                            <a class="nav-link" style="font-family: Nunitosans" href="/shop">SHOP <span class="sr-only">(current)</span></a>
                        </li>
                        <br>
                        <li class="nav-item active">
                            <a class="nav-link" style="font-family: Nunitosans" href="/top_up">TOPUP <span class="sr-only">(current)</span></a>
                        </li>

                    </ul>

                </div>
            </div>
        </nav>
        <main class="py-4">
            <?php echo $__env->yieldContent('navbar1'); ?>
        </main>
        <footer id="myFooter" style="background-color: #0064d2; margin-top:40px;">
  <div class="container-fluid" style="background-color: #0064d2;">
      <div class="row" style="margin-bottom: 15px;">
          <div class="col-12 col-sm-6 col-md-3 col-md-4">
              <img src="img/LOGO.png" alt="gamersspace">
              <p style="font-size: 14px;">Jl. Taman Kelud no 9 Semarang, Jawa Tengah</p>
              <p style="font-size: 14px;">Telepon : (022) 4231737</p>
              <p style="font-size: 14px;">Email : info@gamerspace.go.id</p>
              <ul class="list-inline">
                  <li class="list-inline-item"><a href="#"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fab fa-facebook fa-stack-1x fa-inverse"></i></span></a></li>
                  <li class="list-inline-item"><a href="#"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fab fa-twitter fa-stack-1x fa-inverse"></i></span></a></li>
                  <li class="list-inline-item"><a href="#"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fab fa-instagram fa-stack-1x fa-inverse"></i></span></a></li>
                  <li class="list-inline-item"><a href="#"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fab fa-youtube fa-stack-1x fa-inverse"></i></span></a></li>
              </ul>
          </div>
          <div class="col col-12 col-sm-6 col-md-4" width="100%" height="400" style="padding-left: 40px;padding-right: 40px;padding-top: 0px;">
              <h2 class="text-center" style="font-size: 24px;font-family: Lato, sans-serif;margin-bottom: 36px;margin-top: 8px;"><strong>Maps</strong></h2><iframe allowfullscreen="" frameborder="0" src="https://cdn.bootstrapstudio.io/placeholders/map.html" width="100%" height="200"></iframe></div>
          <div class="col col-12 col-sm-6 col-md-4 site-form">
              <h2 class="text-left" style="font-size: 24px;font-family: Lato, sans-serif;margin-bottom: 15px;margin-top: 0px;"><strong>Hubungi Kami</strong></h2>
              <form id="my-form">
                  <div class="form-group"><label class="sr-only" for="fullname">Full Name</label><input class="form-control" type="text" id="fullname" name="firstname" placeholder="First Name"></div>
                  <div class="form-group"><label class="sr-only" for="email">Email Address</label><input class="form-control" type="text" id="email" name="email" required="" placeholder="Email"></div>
                  <div class="form-group"><label class="sr-only" for="messages">Last Name</label><textarea class="form-control" name="messages" required="" placeholder="Message" rows="8" style="height: 90px;"></textarea></div><button class="btn btn-light btn-lg" id="form-btn"
                      type="submit" style="background-color: rgb(255,255,255);color: rgb(0,0,0);font-family: 'Nunito Sans', sans-serif;font-size: 16px;">Submit</button></form>
          </div>
          <div class="clearfix"></div>
      </div>
      <div class="row footer-copyright" style="background-color: #012e5f;margin-bottom: 0px;">
          <div class="col">
              <p>© 2019 Copyright By Mahasiswa Universitas Diponegoro</p>
          </div>
      </div>
  </div>
</footer>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="<?php echo e(URL::asset('template1/vendor/jquery/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('template1/vendor/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('template2/assets/js/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('assets/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('template2/assets/js/bs-animation.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('template2/https://use.fontawesome.com/1744f3f671.js')); ?>"></script>
</body>
    
</html><?php /**PATH C:\laragon\www\GamersSpace-master\gamerspace\resources\views/layouts/navbar.blade.php ENDPATH**/ ?>