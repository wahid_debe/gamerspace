<?php $__env->startSection('navbar1'); ?>


<div class="container" align="center">
    <div class="col-lg-11">
        <!-- progress bar -->
        <div class="row col-lg-10">
            <div class="circle-bar">
                <span>Order</span>
                <div class="circle active"> </div>
            </div>
            <div class="circle-bar">
                <span>Payment</span>
                <div class="circle"> </div>
            </div>
            <div class="circle-bar">
                <span>Detail Payment</span>
                <div class="circle"> </div>
            </div>
            <div class="circle-bar">
                <span>Done</span>
                <div class="circle"> </div>
            </div>
        </div>

        <!-- content -->
        <form enctype="multipart/form-data" action="<?php echo e(url('/topup/{idg}')); ?>" method="post">
            <?php echo e(csrf_field()); ?>

            <div class="container-order">
                <span>PILIH <?php echo e($game->currency); ?></span>
                <div class="row-order">
                    <?php $__currentLoopData = $topup; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $topup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-3 col-lg-3">
                            <input type="radio" name="price" id="<?php echo e($topup->balance); ?>">
                            <label for="<?php echo e($topup->balance); ?>"><?php echo e($topup->balance); ?> <?php echo e($topup->currency); ?></label>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            <div class="container-order">
                <span>MASUKKAN ID <?php echo e($game->game); ?></span>
                <div class="row-order">
                    <div class="col-md-4 col-lg-4">
                        <p class="button-game-name">MASUKKAN ID</p>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <input type="text" class="button-game-name" name="id_user" placeholder=" (            ) ">
                    </div>
                </div>
                <p class="hint">Untuk mengetahui User ID Anda, Silakan Klik menu profile dibagian kiri atas pada menu utama game. Dan user ID akan terlihat dibagian bawah Nama Karakter Game Anda. Silakan masukkan User ID Anda untuk menyelesaikan transaksi. Contoh : 12345678(1234).</p>
            </div>
            <div class="container-submit">
                    <a class="btn btn-default col-lg-2" href="/top_up">Cancel</a>
                    <a class="btn btn-warning col-lg-2" href="<?php echo e($idg); ?>/payment">Top-Up</a>
            </div>
        </form>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\GamersSpace-master\gamerspace\resources\views/order.blade.php ENDPATH**/ ?>