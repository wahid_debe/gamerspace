<!-- modal-edit -->
<div class="modal modal-edit fade" id="edit-modal-<?php echo e($topup->id_topup); ?>">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<h4 class="modal-game" align="center"><b>Edit Game</b></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form enctype="multipart/form-data" action="<?php echo e(url('/admin/topup/savePrice')); ?>" method="post">
				<?php echo e(csrf_field()); ?>

				<div class="modal-body">
					<div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Game Name</label>
			              </div>
			              <div class="col-sm-9">
			                <input type="text" class="form-control" name="id_game" placeholder="<?php echo e($topup->game); ?>" disabled>
			              </div>
			            </div>
			        </div>
					<div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Balance</label>
			              </div>
			              <div class="col-sm-9">
			                <input type="text" class="form-control" name="balance" value="<?php echo e($topup->balance); ?>" placeholder="<?php echo e($topup->balance); ?>">
			              </div>
			            </div>
			        </div>
					<div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Price</label>
			              </div>
			              <div class="col-sm-9">
			                <input type="text" class="form-control" name="price" value="<?php echo e($topup->price); ?>" placeholder="<?php echo e($topup->price); ?>">
			              </div>
			            </div>
			        </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div><?php /**PATH F:\tugas\DKP\GamersSpace-master\resources\views/admin/topup/editPrice.blade.php ENDPATH**/ ?>