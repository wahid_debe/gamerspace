<!-- modal-edit -->
<div class="modal modal-edit fade" id="edit-modal-<?php echo e($event->id_event); ?>">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<h4 class="modal-title" align="center"><b>Edit Event</b></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form enctype="multipart/form-data" action="<?php echo e(url('/admin/event/save')); ?>" method="post">
				<?php echo e(csrf_field()); ?>

				<div class="modal-body">
					<div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Event Name</label>
			              </div>
			              <div class="col-sm-9">
			              	<input type="hidden" name="id_event" value="<?php echo e($event->id_event); ?>">
			                <input type="text" class="form-control" name="event_name" value="<?php echo e($event->event_name); ?>" placeholder="<?php echo e($event->event_name); ?>">
			              </div>
			            </div>
			        </div>
			        <div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Poster</label>
			              </div>
			              <div class="col-sm-9">
			              	<input type="hidden" name="poster1" value="<?php echo e($event->poster); ?>">
			                <input type="file" name="poster">
			              </div>
			            </div>
			        </div>
			        <div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Description</label>
			              </div>
			              <div class="col-sm-9">
			                <textarea class="textarea" name="description" value="<?php echo e($event->description); ?>" placeholder="<?php echo e($event->description); ?>"
			                style="width: 100%; height: 150px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo e($event->description); ?></textarea>
			              </div>
			            </div>
			        </div>
			        <div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Date</label>
			              </div>
			              <div class="col-sm-9">
			                <input type="date" name="date" value="<?php echo e($event->date); ?>" placeholder="<?php echo e($event->date); ?>">
			              </div>
			            </div>
			        </div>
			        <div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Contact Person</label>
			              </div>
			              <div class="col-sm-9">
			                <input type="text" name="contact_person" value="<?php echo e($event->contact_person); ?>" placeholder="<?php echo e($event->contact_person); ?>">
			              </div>
			            </div>
			        </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div><?php /**PATH C:\laragon\www\GamersSpace-master\gamerspace\resources\views/admin/event/edit.blade.php ENDPATH**/ ?>