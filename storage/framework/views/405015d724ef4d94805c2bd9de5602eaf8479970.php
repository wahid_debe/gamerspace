<!-- modal-delete -->
<div class="modal modal-edit fade" id="delete-modal-<?php echo e($game->id_game); ?>">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<h4 class="modal-title" align="center"><b>Delete game</b></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form enctype="multipart/form-data" action="<?php echo e(url('/admin/topup/deleteGame')); ?>" method="post">
				<?php echo e(csrf_field()); ?>

				<div class="modal-body">
					<input type="hidden" name="id_game" value="<?php echo e($game->id_game); ?>">
					<input type="hidden" name="image" value="<?php echo e($game->image); ?>">
					<h5>Are you sure you want to delete game "<?php echo e($game->game); ?>"</h5>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div><?php /**PATH C:\laragon\www\GamersSpace-master\gamerspace\resources\views/admin/topup/deleteGame.blade.php ENDPATH**/ ?>