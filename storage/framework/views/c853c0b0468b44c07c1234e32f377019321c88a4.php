<!-- modal-delete -->
<div class="modal modal-edit fade" id="delete-modal-<?php echo e($event->id_event); ?>">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<h4 class="modal-title" align="center"><b>Delete Event</b></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form enctype="multipart/form-data" action="<?php echo e(url('/admin/event/delete')); ?>" method="post">
				<?php echo e(csrf_field()); ?>

				<div class="modal-body">
					<input type="hidden" name="id_event" value="<?php echo e($event->id_event); ?>">
					<input type="hidden" name="poster" value="<?php echo e($event->poster); ?>">
					<h5>Are you sure you want to delete event "<?php echo e($event->event_name); ?>"</h5>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div><?php /**PATH C:\laragon\www\GamersSpace\resources\views/admin/event/delete.blade.php ENDPATH**/ ?>