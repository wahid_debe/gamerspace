<?php $__env->startSection('content'); ?>

<div class="card mb-3" style="width: 100%">
  <div class="card-header">
    <i class="fas fa-table"></i>
    Sales Transaction</div>
  <div class="card-body">
    <div class="table-responsive table-striped">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Transaction ID</th>
                <th>Price</th>
                <th>Create Date</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Transaction ID</th>
                <th>Price</th>
                <th>Create Date</th>
            </tr>
        </tfoot>
        <tbody>
            <?php $__currentLoopData = $sale; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sale): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($sale->id_transaction); ?></td>
                <td><?php echo e($sale->price); ?></td>
                <td><?php echo e($sale->created_at); ?></td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="card-footer small text-muted">Gamers Space</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.navbar_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\GamersSpace\resources\views/admin/saletransaction/view.blade.php ENDPATH**/ ?>