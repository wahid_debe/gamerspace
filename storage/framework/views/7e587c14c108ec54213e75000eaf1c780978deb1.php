<?php $__env->startSection('navbar1'); ?>


<div class="container" align="center">
    <div class="col-sm-11">
        <!-- progress bar -->
        <div class="row col-sm-10">
            <div class="circle-bar">
                <span>Order</span>
                <div class="circle"> </div>
            </div>
            <div class="circle-bar">
                <span>Payment</span>
                <div class="circle active"> </div>
            </div>
            <div class="circle-bar">
                <span>Detail Payment</span>
                <div class="circle"> </div>
            </div>
            <div class="circle-bar">
                <span>Done</span>
                <div class="circle"> </div>
            </div>
        </div>
        <br>
        <!-- content -->
        <div class="col-sm-9">
            <h4 align="left">CHECKOUT DETAILS</h4>
            <div class="container-order">
                <span>Profile Buyer</span>
                <p class="hint">Name</p>
                <div class="form-group">
                    <input type="text" name="nama" class="form-control" placeholder="Please input your name">
                </div>
                <p class="hint">Email</p>
                <div class="form-group">
                    <input type="text" name="nama" class="form-control" placeholder="Please input your email">
                </div>
                <p class="hint">Your Phone Number</p>
                <div class="form-group">
                    <input type="text" name="nama" class="form-control" placeholder="Please input your phone number">
                </div>
            </div>
            <br>
            <h4 align="left">CHOOSE PAYMENT</h4>
            <div class="container-order">
                <p class="Belum ada"></p>
            </div>

            <div class="container-submit">
                    <a class="btn btn-default col-lg-2" href="/top_up/<?php echo e($idg); ?>">Back</a>
                    <a class="btn btn-warning col-lg-2" href="<?php echo e($idg); ?>/detail_payment">Top-Up</a>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\GamersSpace-master\gamerspace\resources\views/payment.blade.php ENDPATH**/ ?>