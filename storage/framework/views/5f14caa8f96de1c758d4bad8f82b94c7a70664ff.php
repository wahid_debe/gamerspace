<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GAME SPACE - Dashboard</title>

    <!-- Custom fonts for this template-->
    <link rel="stylesheet"  href="<?php echo e(URL::asset('template/vendor/fontawesome-free/css/all.min.css')); ?>">

    <!-- Page level plugin CSS-->
    <link rel="stylesheet" href="<?php echo e(URL::asset('template/vendor/datatables/dataTables.bootstrap4.css')); ?>">

    <!-- Custom styles for this template-->
    <link rel="stylesheet" href="<?php echo e(URL::asset('template/css/sb-admin.css')); ?>">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="/admin">Game Space</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
        <div class="input-group-append">
          <button class="btn btn-primary" type="button">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>

  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <?php 
    function activebar($un){
        $S1 = Session::get('active');
        if ($S1==$un) echo "active";
    }
    ?>

    <ul class="sidebar navbar-nav">
      <li class="nav-item <?php activebar(1);?>">
        <a class="nav-link" href="/admin">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>
      </li>
      <li class="nav-item <?php activebar(2);?>">
        <a class="nav-link" href="/admin/event">
          <i class="fas fa-fw fa-table"></i>
          <span>Event</span></a>
      </li>
      <li class="nav-item <?php activebar(3);?>">
        <a class="nav-link" href="/admin/news">
          <i class="fas fa-fw fa-table"></i>
          <span>News</span></a>
      </li>
      <li class="nav-item <?php activebar(4);?>">
        <a class="nav-link" href="/admin/topup">
          <i class="fas fa-fw fa-table"></i>
          <span>Top Up</span></a>
      </li>
      <li class="nav-item <?php activebar(5);?>">
        <a class="nav-link" href="/admin/sales_transaction">
          <i class="fas fa-fw fa-table"></i>
          <span>Sale Transactions</span></a>
      </li>
    </ul>
        <?php echo $__env->yieldContent("content"); ?>
        <!-- <script src="<?php echo e(URL::asset('DataTables/js/datatables.min.js')); ?>"></script> -->
        <!-- <script type="text/javascript" src="<?php echo e(URL::asset('https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js')); ?>"></script> -->
        <!-- jQuery 3 -->
        <!-- <script src="<?php echo e(URL::asset('jquery/dist/jquery.min.js')); ?>"></script> -->
        <!-- Bootstrap 3.3.7 --><!-- 
        <script src="<?php echo e(URL::asset('bootstrap/dist/js/bootstrap.min.js')); ?>"></script> -->
        <!-- dataTable -->
        <!-- <script src="<?php echo e(URL::asset('datatables.net-bs/js/dataTables.bootstrap.min.js')); ?>"></script>
        <script type="text/javascript">
            $(function () {
                $('#dataTable').DataTable({
                })
            })
        </script>  -->

      <!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Game Space 2019</span>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo e(URL::asset('template/vendor/jquery/jquery.min.js')); ?>"></script>
  <script src="<?php echo e(URL::asset('template/vendor/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo e(URL::asset('template/vendor/jquery-easing/jquery.easing.min.js')); ?>"></script>

  <!-- Page level plugin JavaScript-->
  <script src="<?php echo e(URL::asset('template/vendor/chart.js/Chart.min.js')); ?>"></script>
  <script src="<?php echo e(URL::asset('template/vendor/datatables/jquery.dataTables.js')); ?>"></script>
  <script src="<?php echo e(URL::asset('template/vendor/datatables/dataTables.bootstrap4.js')); ?>"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo e(URL::asset('template/js/sb-admin.min.js')); ?>"></script>

  <!-- Demo scripts for this page-->
  <script src="<?php echo e(URL::asset('template/js/demo/datatables-demo.js')); ?>"></script>
  <script src="<?php echo e(URL::asset('template/js/demo/chart-area-demo.js')); ?>"></script>  
</body>

</html>
<?php /**PATH C:\laragon\www\GamersSpace-master\gamerspace\resources\views/layouts/navbar_admin.blade.php ENDPATH**/ ?>