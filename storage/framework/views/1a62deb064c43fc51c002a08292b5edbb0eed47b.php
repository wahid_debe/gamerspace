<?php $__env->startSection('content'); ?>

<div class="card mb-3" style="width: 100%">
  <div class="card-header">
    <i class="fas fa-table"></i>
    Add Top Up Price</div>
  <div class="card-body">
	<form enctype="multipart/form-data" action="<?php echo e(url('/admin/topup/storePrice')); ?>" method="post">
		<?php echo e(csrf_field()); ?>

		<div class="modal-body">
			<div class="form-group">
	            <div class="form-row">
	              <div class="col-sm-3">
	                <label>Game Name</label>
	              </div>
	              <div class="col-sm-9">
	                <select name="id_game" class="form-control select2" style="width: 100%;" data-error="Maaf, pertemuan tidak boleh kosong." required>
                        <option disabled selected value>-</option>
                        <?php $__currentLoopData = $game->sortBy('id_game'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $game): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($game->id_game); ?>"><?php echo e($game->game); ?><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></option>
                    </select>
	              </div>
	            </div>
	        </div>
	        <div class="form-group">
	            <div class="form-row">
	              <div class="col-sm-3">
	                <label>Balance</label>
	              </div>
	              <div class="col-sm-9">
	                <input type="number" class="form-control" name="balance" required>
	              </div>
	            </div>
	        </div>
	        <div class="form-group">
	            <div class="form-row">
	              <div class="col-sm-3">
	                <label>Price</label>
	              </div>
	              <div class="col-sm-9">
	                <input type="number" class="form-control" name="price" required>
	              </div>
	            </div>
	        </div>
		</div>
        <div class="modal-footer">
            <a href="/admin/topup"><button type="button" class="btn btn-default pull-left">Close</button></a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
	</form>
  </div>
  <div class="card-footer small text-muted">Gamers Space</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.navbar_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\GamersSpace\resources\views/admin/topup/addPrice.blade.php ENDPATH**/ ?>