<!-- modal-edit -->
<div class="modal modal-edit fade" id="edit-modal-<?php echo e($news->id_news); ?>">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<h4 class="modal-title" align="center"><b>Edit News</b></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form enctype="multipart/form-data" action="<?php echo e(url('/admin/news/save')); ?>" method="post">
				<?php echo e(csrf_field()); ?>

				<div class="modal-body">
					<div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Title</label>
			              </div>
			              <div class="col-sm-9">
			              	<input type="hidden" name="id_news" value="<?php echo e($news->id_news); ?>">
			                <input type="text" class="form-control" name="title" value="<?php echo e($news->title); ?>" placeholder="<?php echo e($news->title); ?>">
			              </div>
			            </div>
			        </div>
			        <div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Image</label>
			              </div>
			              <div class="col-sm-9">
			              	<input type="hidden" name="image1" value="<?php echo e($news->image); ?>">
			                <input type="file" name="image">
			              </div>
			            </div>
			        </div>
			        <div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Description</label>
			              </div>
			              <div class="col-sm-9">
			                <textarea class="textarea" name="description" value="<?php echo e($news->description); ?>" placeholder="<?php echo e($news->description); ?>"
			                style="width: 100%; height: 150px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo e($news->description); ?></textarea>
			              </div>
			            </div>
			        </div>
			        <div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Date</label>
			              </div>
			              <div class="col-sm-9">
			                <input type="date" name="date" value="<?php echo e($news->date); ?>" placeholder="<?php echo e($news->date); ?>">
			              </div>
			            </div>
			        </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div><?php /**PATH C:\laragon\www\GamersSpace\resources\views/admin/news/edit.blade.php ENDPATH**/ ?>