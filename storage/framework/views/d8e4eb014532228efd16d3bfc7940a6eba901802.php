<?php $__env->startSection('content'); ?>

<!-- content -->
<div class="card mb-3" style="width: 100%" >
  <div class="card-header">
    <i class="fas fa-table"></i>
    Event</div>
  <div class="card-body">
    <a class="btn btn-primary btn-block" style="margin-bottom: 25px" href="/admin/event/add"><i class="fa fa-fw fa-plus"></i><b> Add New Event</b></a>
    <div class="table-responsive table-striped">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Poster</th>
                <th>Event Name</th>
                <th>Description</th>
                <th>Date</th>
                <th>Contact Person</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Poster</th>
                <th>Event Name</th>
                <th>Description</th>
                <th>Date</th>
                <th>Contact Person</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php $__currentLoopData = $event; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td width="155px"><img src="<?php echo e(asset('uploads/event/'.$event->poster)); ?>" width="150px" height="200px"></td>
                <td><?php echo e($event->event_name); ?></td>
                <td><textarea  placeholder="<?php echo e($event->description); ?>" style="width: 100%; height: 150px; border: 0; background:rgba(0,0,0,0); resize: none;" disabled></textarea></td>
                <td><?php echo e($event->date); ?></td>
                <td><?php echo e($event->contact_person); ?></td>
                <td>
                    <span data-toggle="modal" data-target="#edit-modal-<?php echo e($event->id_event); ?>"><a href="#" data-placement="top" data-original-title="Ubah Data" data-toggle="tooltip" class="btn btn-xs btn-success"><i class="fa fa-fw fa-edit"></i> <b class="hover-effect">Edit</b></a></span>
                    <?php echo $__env->make("admin.event.edit", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <span data-toggle="modal" data-target="#delete-modal-<?php echo e($event->id_event); ?>" data-username="<?php echo e($event->event_name); ?>" data-id="<?php echo e($event->id_event); ?>"><a href="#" data-placement="top" data-original-title="Delete Event" data-toggle="tooltip" class="btn btn-xs btn-danger"><i class="fa fa-fw fa-times"></i> <b class="hover-effect">Delete</b></a></span></td>
                    <?php echo $__env->make("admin.event.delete", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="card-footer small text-muted">Gamers Space</div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.navbar_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\GamersSpace\resources\views/admin/event/view.blade.php ENDPATH**/ ?>