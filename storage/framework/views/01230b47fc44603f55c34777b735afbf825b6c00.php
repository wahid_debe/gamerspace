<?php $__env->startSection('navbar1'); ?>

<div class="container" align="center">
    <div class="col-sm-11">
        <div class="row">
            <div class="col-sm-9" align="left">
                <h4 >TOP UP</h4>
            </div>
            <div class="col-sm-3" align="right">
                <form action="/top_up/search" method="GET">
                    <div class="inner-addon left-addon">
                        <i class="glyphicon glyphicon-user"></i> 
                        <input class="form-control" type="search" name="search" placeholder="Search Game">
                    </div>
                </form>
                <br>
            </div>
        </div>
        <div class="row justify-content-center">
                <?php $__currentLoopData = $game; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $game): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-sm-3">
    					<a class="card-text" href="top_up/<?php echo e($game->id_game); ?>">
                        <img class="card-img-top img-responsive rounded" src="<?php echo e(asset('uploads/icon/'.$game->image)); ?>" alt="Game Icon">
    					<div class="card-body">
    						<center><b><?php echo e($game->game); ?></b></center>
    					</div>
                        </a>
    				</div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\GamersSpace-master\gamerspace\resources\views/top_up.blade.php ENDPATH**/ ?>