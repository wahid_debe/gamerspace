<?php $__env->startSection('navbar1'); ?>

<div class="container-new">
	<div class="bottom">
		<div class="col-md-12">
			<div class="col-lg-8 col-md-8 col-sm-12 left-side">
				<div class="title">
					<p><?php echo e($select_news->date); ?></p>
					<h4><?php echo e($select_news->title); ?></h4>
					<p>author of article: Alvis Elian</p>
				</div>
				<div class="margin-top">
					<img src="<?php echo e(asset('uploads/news/'.$select_news->image)); ?>" class="card-img-top" alt="...">
				</div>
				<div class="margin-top">
					<p><?php echo e($select_news->description); ?></p>	
				</div>
				
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 right-side">
				<div class="col-sm-12 news3">
					<ul class="nav nav-tabs">
					    <li><a class="active" data-toggle="tab" href="#populer"><b>TERPOPULER</b></a></li>
						<li><a data-toggle="tab" href="#baru"><b>TERBARU</b></a></li>
					</ul>

					<div class="tab-content">
					    <div id="populer" class="tab-pane active">
					    	<?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $news3): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					    	<div class="group">
						    	<div class="col-lg-4 col-md-12 col-sm-6 fl-left">
									<img src="<?php echo e(asset('uploads/news/'.$news3->image)); ?>" class="card-img-top" alt="...">
								</div>
								<div class="col-lg-8 col-md-12 col-sm-6 fl-left">
									<h4 class="card-title"><b><?php echo e($news3->title); ?></b></h4>
									<div class="footer-news">
										<img src="/img/clock.png" alt="...">
										<p><?php echo e($news3->date); ?></p>
										<img src="/img/eye.png" alt="...">
										<p>100 views</p>
									</div>
								</div>
							</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					    </div>
					    <div id="baru" class="tab-pane fade">
					    	<?php $__currentLoopData = $news->sortBy('date'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $news4): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					    	<div class="group">
						    	<div class="col-lg-4 col-sm-12 fl-left">
									<img src="<?php echo e(asset('uploads/news/'.$news4->image)); ?>" class="card-img-top" alt="...">
								</div>
								<div class="col-lg-8 col-sm-12 fl-left">
									<h4 class="card-title"><b><?php echo e($news4->title); ?></b></h4>
									<div class="footer-news">
										<img src="/img/clock.png" alt="...">
										<p><?php echo e($news4->date); ?></p>
										<img src="/img/eye.png" alt="...">
										<p>100 views</p>
									</div>
								</div>
							</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					    </div>
					</div>
				</div>
				<div class="white-box">___</div>
				<div class="news1">
					<div class="write-text">
						<?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $news1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<div class="col-lg-12 col-sm-12 group">
							<h4 class="card-title"><b>ANDROID BERITA IOS PLATFORM</b></h4>
							<p class="card-text"><b><?php echo e($news1->title); ?></b></p>
							<div class="footer-news">
								<img src="/img/clock.png" alt="...">
								<p><?php echo e($news1->date); ?></p>
							</div>
						</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			    	</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\GamersSpace-master\gamerspace\resources\views/detailsNews.blade.php ENDPATH**/ ?>