<?php $__env->startSection('content'); ?>

<div class="card mb-3" style="width: 100%">
  <div class="card-header">
    <i class="fas fa-table"></i>
    Add Game</div>
  <div class="card-body">
	<form enctype="multipart/form-data" action="<?php echo e(url('/admin/topup/storeGame')); ?>" method="post">
		<?php echo e(csrf_field()); ?>

		<div class="modal-body">
			<div class="form-group">
	            <div class="form-row">
	              <div class="col-sm-3">
	                <label>Game Name</label>
	              </div>
	              <div class="col-sm-9">
	                <input type="text" class="form-control" name="game" placeholder="enter game name" required>
	              </div>
	            </div>
	        </div>
	        <div class="form-group">
	            <div class="form-row">
	              <div class="col-sm-3">
	                <label>Game Icon</label>
	              </div>
	              <div class="col-sm-9">
	                <input type="file" name="image" required>
	              </div>
	            </div>
	        </div>
	        <div class="form-group">
	            <div class="form-row">
	              <div class="col-sm-3">
	                <label>Game Currency</label>
	              </div>
	              <div class="col-sm-9">
	                <input type="text" class="form-control" name="currency" placeholder="enter game name" required>
	              </div>
	            </div>
	        </div>
		</div>
        <div class="modal-footer">
            <a href="/admin/topup"><button type="button" class="btn btn-default pull-left">Close</button></a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
	</form>
  </div>
  <div class="card-footer small text-muted">Gamers Space</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.navbar_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\GamersSpace\resources\views/admin/topup/addGame.blade.php ENDPATH**/ ?>