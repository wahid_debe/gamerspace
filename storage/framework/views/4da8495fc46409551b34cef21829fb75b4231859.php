<!-- modal-add-game -->
<div class="modal modal-edit fade" id="add-game-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<h4 class="modal-title" align="center"><b>Add Game</b></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form enctype="multipart/form-data" action="<?php echo e(url('/admin/news/save')); ?>" method="post">
				<?php echo e(csrf_field()); ?>

				<div class="modal-body">
					<div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Game Name</label>
			              </div>
			              <div class="col-sm-9">
			                <input type="text" class="form-control" name="game" placeholder="enter game name" required>
			              </div>
			            </div>
			        </div>
			        <div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Game Icon</label>
			              </div>
			              <div class="col-sm-9">
			                <input type="file" name="image" required>
			              </div>
			            </div>
			        </div>
			        <div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Game Currency</label>
			              </div>
			              <div class="col-sm-9">
			                <input type="text" class="form-control" name="currency" placeholder="enter game name" required>
			              </div>
			            </div>
			        </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- modal-add-price -->
<div class="modal modal-edit fade" id="add-price-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<h4 class="modal-title" align="center"><b>Add Price</b></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form enctype="multipart/form-data" action="<?php echo e(url('/admin/news/save')); ?>" method="post">
				<?php echo e(csrf_field()); ?>

				<div class="modal-body">
					<div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Game Name</label>
			              </div>
			              <div class="col-sm-9">
			                <select name="id_pertemuan" class="form-control select2" style="width: 100%;" data-error="Maaf, pertemuan tidak boleh kosong." required>
                                <option disabled selected value>-</option>
                                <option value="232">11</option>
                                <option value="2322">113</option>
                                <option value="2323">111</option>
                            </select>
			              </div>
			            </div>
			        </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div><?php /**PATH C:\laragon\www\GamersSpace\resources\views/admin/topup/add.blade.php ENDPATH**/ ?>