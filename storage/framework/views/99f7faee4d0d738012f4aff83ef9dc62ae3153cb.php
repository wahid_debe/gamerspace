<?php $__env->startSection('content'); ?>

<!-- content -->
<div class="card mb-3" style="width: 100%" >
  <div class="card-header">
    <i class="fas fa-table"></i>
    News</div>
  <div class="card-body">
    <a class="btn btn-primary btn-block" style="margin-bottom: 25px" href="/admin/news/add"><i class="fa fa-fw fa-plus"></i><b> Add News</b></a>
    <div class="table-responsive table-striped">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Image</th>
                <th>Title</th>
                <th>Description</th>
                <th>Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Image</th>
                <th>Title</th>
                <th>Description</th>
                <th>Date</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $news): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td width="155px"><img src="<?php echo e(asset('uploads/news/'.$news->image)); ?>" width="150px" height="200px"></td>
                <td><?php echo e($news->title); ?></td>
                <td><?php echo e($news->description); ?></td>
                <td><?php echo e($news->date); ?></td>
                <td>
                    <span data-toggle="modal" data-target="#edit-modal-<?php echo e($news->id_news); ?>"><a href="#" data-placement="top" data-original-title="Edit News" data-toggle="tooltip" class="btn btn-xs btn-success"><i class="fa fa-fw fa-edit"></i> <b class="hover-effect">Edit</b></a></span>
                    <?php echo $__env->make("admin.news.edit", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <span data-toggle="modal" data-target="#delete-modal-<?php echo e($news->id_news); ?>" data-username="<?php echo e($news->title); ?>" data-id="<?php echo e($news->id_news); ?>"><a href="#" data-placement="top" data-original-title="Delete News" data-toggle="tooltip" class="btn btn-xs btn-danger"><i class="fa fa-fw fa-times"></i> <b class="hover-effect">Delete</b></a></span></td>
                    <?php echo $__env->make("admin.news.delete", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="card-footer small text-muted">Gamers Space</div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.navbar_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\GamersSpace\resources\views/admin/news/view.blade.php ENDPATH**/ ?>