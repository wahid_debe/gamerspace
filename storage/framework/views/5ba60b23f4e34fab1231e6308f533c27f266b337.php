<?php $__env->startSection('content'); ?>

<div class="card mb-3" style="width: 100%">
  <div class="card-header">
    <i class="fas fa-table"></i>
    Edit Game</div>
  <div class="card-body">
    <a class="btn btn-primary col-sm-12" style="margin-bottom: 10px;" href="/admin/topup/addGame"><i class="fa fa-fw fa-plus"></i><b> Add New Game</b></a>
    <a class="btn btn-primary col-sm-12" style="margin-bottom: 10px;" href="/admin/topup"><b> Back</b></a>
	<div class="table-responsive table-striped">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Image</th>
                <th>Game Name</th>
                <th>Currency</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Image</th>
                <th>Game Name</th>
                <th>Currency</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            <?php $__currentLoopData = $game; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $game): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td width="155px"><img src="<?php echo e(asset('uploads/icon/'.$game->image)); ?>" width="150px" height="200px"></td>
                <td><?php echo e($game->game); ?></td>
                <td><?php echo e($game->currency); ?></td>
                <td>
                    <span data-toggle="modal" data-target="#edit-modal-<?php echo e($game->id_game); ?>"><a href="#" data-placement="top" data-original-title="Edit game" data-toggle="tooltip" class="btn btn-xs btn-success"><i class="fa fa-fw fa-edit"></i> <b class="hover-effect">Edit</b></a></span>
                    <?php echo $__env->make("admin.topup.editGame", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <span data-toggle="modal" data-target="#delete-modal-<?php echo e($game->id_game); ?>" data-username="<?php echo e($game->game); ?>" data-id="<?php echo e($game->id_game); ?>"><a href="#" data-placement="top" data-original-title="Delete game" data-toggle="tooltip" class="btn btn-xs btn-danger"><i class="fa fa-fw fa-times"></i> <b class="hover-effect">Delete</b></a></span></td>
                    <?php echo $__env->make("admin.topup.deleteGame", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="card-footer small text-muted">Gamers Space</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.navbar_admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\GamersSpace\resources\views/admin/topup/viewGame.blade.php ENDPATH**/ ?>