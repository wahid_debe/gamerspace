<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopUp extends Model
{
	protected $primaryKey = 'id_topup';
    protected $table = 'topups';
}
