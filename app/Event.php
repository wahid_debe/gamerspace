<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
	protected $primaryKey = 'id_event';
    protected $table = 'events';
}
