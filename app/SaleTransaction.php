<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleTransaction extends Model
{
    protected $primaryKey = 'id_transaction';
    protected $table = 'saletransactions';
}
