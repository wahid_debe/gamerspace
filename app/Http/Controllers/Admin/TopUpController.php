<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Game;
use App\Topup;
use Redirect;
use File;
use Illuminate\SUpport\Facades\DB;
use Session;

class TopUpController extends Controller
{
    //View TopUp
    public function viewTopUp()
    {
        Session::flash('active','4');
        $games = Game::all();
        $topup = DB::table('topups')->join('games', function ($join) {$join->on('games.id_game','=','topups.id_game');})->select('*')->get();
        return view('admin.topup.view',['topup' => $topup]);
    }

    //View Game
    public function viewGame()
    {
        Session::flash('active','4');
        $games = DB::table('games')->get();
        return view('admin.topup.viewGame',['game' => $games]);
    }

    // Add Game
    public function addGame()
    {
        return view('admin.topup.addGame');
    }

    // Add Price
    public function addPrice()
    {
    	$games = DB::table('games')->get();
        return view('admin.topup.addPrice',['game' => $games]);
    }

    // Simpan Penambahan Game
    public function storeGame(Request $request)
    {
        // dd($request);
        $games = new Game();
        $games->game = $request->game;
        $games->image = ' ';
        $games->currency = $request->currency;
        $games->save();
        $games = Game::findOrFail($games->id_game);
        $image = $request->image;
        $file_name = $games->id_game.'-'.$games->game.'.'.$image->getClientOriginalExtension();
        $image->move('uploads/icon/',$file_name);
        $games->image = $file_name;
        $games->save();
        return redirect('/admin/topup');
    }

    // Simpan Penambahan Price
    public function storePrice(Request $request)
    {
        // dd($request);
        $topups = new TopUp();
        $topups->id_game = $request->id_game;
        $topups->price = $request->price;
        $topups->balance = $request->balance;
        $topups->save();
        return redirect('/admin/topup');
    }

    // Ubah Game
    public function saveGame(Request $request)
    {   
        // dd($request);
        $games = DB::table('games')->select('*')->where('id_game','=',$request->id_game)->update([
            'game' => $request->game,
            'currency' => $request->currency
        ]);
        if ($request->image) {
            unlink( "uploads/icon/".$request->image1);
            $games = Game::findOrFail($request->id_game);
            $image = $request->image;
            $file_name = $games->id_game.'-'.$games->game.'.'.$image->getClientOriginalExtension();
            $image->move('uploads/icon/',$file_name);
            $games->image = $file_name;
            $games->save();
        }
        return redirect('/admin/topup/game');
    }

    // Ubah Top Up Price
    public function savePrice(Request $request)
    {   
        // dd($request);
        $topup = DB::table('topups')->select('*')->where('id_game','=',$request->id_game)->update([
            'balance' => $request->balance,
            'price' => $request->price
        ]);
        return redirect('/admin/topup');
    }

    // Hapus Game
    public function deleteGame(Request $request)
    {
        $games = DB::table('games')->select('*')->where('id_game','=',$request->id_game)->delete();
        unlink( "uploads/icon/".$request->image);
        return redirect('/admin/topup/game');
    }

    // Hapus Top Up
    public function deletePrice(Request $request)
    {
        $topups = DB::table('topups')->select('*')->where('id_topup','=',$request->id_topup)->delete();
        return redirect('/admin/topup');
    }
}
