<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SaleTransaction;
use Redirect;
use File;
use Illuminate\SUpport\Facades\DB;
use Session;

class SaleTransactionController extends Controller
{
    // View Sale Transaction
    public function viewSaleTransaction()
    {

        Session::flash('active','5');
        $sale = DB::table('saletransactions')->get();
        return view('admin.saletransaction.view',['sale' => $sale]);
    }
}
