<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use Redirect;
use File;
use Illuminate\Support\Facades\DB;
use Session;

class NewsController extends Controller
{
    // View News
    public function viewNews()
    {
        Session::flash('active','3');
        $news = DB::table('newss')->get();
        return view('admin.news.view',['news' => $news]);
    }

    // Add News
    public function addNews()
    {
        return view('admin.news.add');
    }

    // Simpan Penambahan News
    public function storeNews(Request $request)
    {
        // dd($request);
        $newss = new News();
        $newss->title = $request->title;
        $newss->image = ' ';
        $newss->description = $request->description;
        $newss->date = $request->date;
        $newss->save();
        $newss = News::findOrFail($newss->id_news);
        $image = $request->image;
        $file_name = $newss->id_news.'-'.$newss->title.'.'.$image->getClientOriginalExtension();
        $image->move('uploads/news/',$file_name);
        $newss->image = $file_name;
        $newss->save();
        return redirect('/admin/news');
    }

    // Ubah News
    public function saveNews(Request $request)
    {   
        // dd($request);
        $newss = DB::table('newss')->select('*')->where('id_news','=',$request->id_news)->update([
            'title' => $request->title,
            'description' => $request->description,
            'date' => $request->date
        ]);
        if ($request->image) {
            unlink( "uploads/news/".$request->image1);
            $newss = News::findOrFail($request->id_news);
            $image = $request->image;
            $file_name = $newss->id_news.'-'.$newss->title.'.'.$image->getClientOriginalExtension();
            $image->move('uploads/news/',$file_name);
            $newss->image = $file_name;
            $newss->save();
        }
        return redirect('/admin/news');
    }

    // Hapus News
    public function deleteNews(Request $request)
    {
        $newss = DB::table('newss')->select('*')->where('id_news','=',$request->id_news)->delete();
        unlink( "uploads/news/".$request->image);
        return redirect('/admin/news');
    }
}
