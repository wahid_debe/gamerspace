<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class AdminController extends Controller
{
    public function index()
    {
    	Session::flash('active','1');
        return view('admin/dashboard');
    }
}
