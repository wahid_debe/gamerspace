<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use Redirect;
use File;
use Illuminate\SUpport\Facades\DB;
use Session;

class EventController extends Controller
{
    // View Event
    public function viewEvent()
    {
        Session::flash('active','2');
        $event = DB::table('events')->get();
        return view('admin.event.view',['event' => $event]);
    }

    // Add Event
    public function addEvent()
    {
        return view('admin.event.add');
    }

    // Simpan Penambahan Event
    public function storeEvent(Request $request)
    {
        // dd($request);
        $events = new Event();
        $events->event_name = $request->event_name;
        $events->poster = ' ';
        $events->description = $request->description;
        $events->date = $request->date;
        $events->contact_person = $request->contact_person;
        $events->save();
        $events = Event::findOrFail($events->id_event);
        $poster = $request->poster;
        $file_name = $events->id_event.'-'.$events->event_name.'.'.$poster->getClientOriginalExtension();
        $poster->move('uploads/event/',$file_name);
        $events->poster = $file_name;
        $events->save();
        return redirect('/admin/event');
    }

    // Ubah Event
    public function saveEvent(Request $request)
    {   
        // dd($request);
        $events = DB::table('events')->select('*')->where('id_event','=',$request->id_event)->update([
            'event_name' => $request->event_name,
            'description' => $request->description,
            'date' => $request->date,
            'contact_person' => $request->contact_person
        ]);
        if ($request->poster) {
            unlink( "uploads/event/".$request->poster1);
            $events = Event::findOrFail($request->id_event);
            $poster = $request->poster;
            $file_name = $events->id_event.'-'.$events->event_name.'.'.$poster->getClientOriginalExtension();
            $poster->move('uploads/event/',$file_name);
            $events->poster = $file_name;
            $events->save();
        }
        return redirect('/admin/event');
    }

    // Hapus Event
    public function deleteEvent(Request $request)
    {
        $events = DB::table('events')->select('*')->where('id_event','=',$request->id_event)->delete();
        unlink( "uploads/event/".$request->poster);
        return redirect('/admin/event');
    }
}
