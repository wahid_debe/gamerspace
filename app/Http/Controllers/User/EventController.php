<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use Redirect;
use File;
use Illuminate\SUpport\Facades\DB;
use Session;

class EventController extends Controller
{
    // View Event
    public function viewEvent()
    {
        $event = DB::table('events')->get();
        return view('events',compact('event'));
    }
}
