<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use Redirect;
use File;
use Str;
use Illuminate\Support\Facades\DB;
use Session;

class NewsController extends Controller
{
	//View News
    public function viewNews()
    {
        $news = DB::table('newss')->get();
        foreach ($news as $key => $value) {
        	$news[$key]->description =Str::limit($news[$key]->description,180);
        }
        return view('news',compact('news'));
    }

    //Detail News
    public function detailsNews($idn)
    {	
    	$GLOBALS['idn'] = $idn;
    	$select_news = DB::table('newss')->where('id_news','=',$GLOBALS['idn'])->first();
        $news = DB::table('newss')->get();
    	return view('detailsNews',compact('news', 'select_news'));
    }
}