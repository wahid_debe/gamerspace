<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Game;
use App\Topup;
use Redirect;
use File;
use Illuminate\Support\Facades\DB;
use Session;

class TopUpController extends Controller
{
	//View Game
    public function viewGame()
    {
        Session::flash('active','4');
        $games = DB::table('games')->get();
    	return view('top_up',['game' => $games]);
    }

    //Search Game
    public function search(Request $request)
	{
		$search = $request->search;
		$games = DB::table('games')->where('game','like',"%".$search."%")->get();
		return view('top_up',['game' => $games]);
 
	}

	//Order
	public function orderGame($idg)
    {
    	$GLOBALS['idg'] = $idg;
        $game = DB::table('games')->where('id_game','=',$GLOBALS['idg'])->first();
        $games = Game::all();
        $topup = DB::table('topups')->join('games', function ($join) {$join->on('games.id_game','=','topups.id_game');})->select('*')->where('topups.id_game','=',$GLOBALS['idg'])->get();
    	return view('order',compact('game','topup','idg'));
    }

    //Payment
    public function paymentGame($idg){
    	return view('payment',compact('idg'));
    }
}