<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id_user');
            $table->string('name')->nullable();
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('level')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert(array(
            /*bisa diganti buat idnya admin*/
            'name' => 'irvan syaefulloh',
            'email' => 'irvanrifqi5@gmail.com',
            /*password= 12345678*/
            'password' => '$2y$10$Y4a84KykktDrw7JCYjhVduTkYtWw8ZquWFXbarZI3XDvd8WUH/hp2',
            'level' => ' '
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
