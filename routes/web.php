<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//User
//Home
Route::get('/', function () {
    $games = DB::table('games')->paginate(3);
    $news = DB::table('newss')->paginate(3);
    return view('welcome',compact('games','news'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('news', function () {
    return view('news');
});
//News
Route::get('/news', 'User\NewsController@viewNews');
Route::get('/news/{idn}', 'User\NewsController@detailsNews');
//TopUp
Route::get('/top_up', 'User\TopUpController@viewGame');
Route::get('/top_up/search', 'User\TopUpController@search');
Route::get('/top_up/{idg}', 'User\TopUpController@orderGame');
Route::get('/top_up/{idg}/payment', 'User\TopUpController@paymentGame');
Route::get('/top_up/{idg}/detail_payment', 'User\TopUpController@detaiPaymentGame');
//Event
Route::get('/events', 'User\EventController@viewEvent');

Route::get('premium', function () {
    return view('premium');
});


//Admin
//Dasboard
Route::get('/admin', 'Admin\AdminController@index')->name('home');
//Event
Route::get('/admin/event', 'Admin\EventController@viewEvent');
Route::get('/admin/event/add', 'Admin\EventController@addEvent');
Route::post('/admin/event/store', 'Admin\EventController@storeEvent');
Route::post('/admin/event/save', 'Admin\EventController@saveEvent');
Route::post('/admin/event/delete', 'Admin\EventController@deleteEvent');
//News
Route::get('/admin/news', 'Admin\NewsController@viewNews');
Route::get('/admin/news/add', 'Admin\NewsController@addNews');
Route::post('/admin/news/store', 'Admin\NewsController@storeNews');
Route::post('/admin/news/save', 'Admin\NewsController@saveNews');
Route::post('/admin/news/delete', 'Admin\NewsController@deleteNews');
//TopUp
Route::get('/admin/topup', 'Admin\TopUpController@viewTopUp');
Route::get('/admin/topup/game', 'Admin\TopUpController@viewGame');
Route::get('/admin/topup/addGame	', 'Admin\TopUpController@addGame');
Route::get('/admin/topup/addPrice', 'Admin\TopUpController@addPrice');
Route::post('/admin/topup/storeGame', 'Admin\TopUpController@storeGame');
Route::post('/admin/topup/storePrice', 'Admin\TopUpController@storePrice');
Route::post('/admin/topup/saveGame', 'Admin\TopUpController@saveGame');
Route::post('/admin/topup/savePrice', 'Admin\TopUpController@savePrice');
Route::post('/admin/topup/deleteGame', 'Admin\TopUpController@deleteGame');
Route::post('/admin/topup/deletePrice', 'Admin\TopUpController@deletePrice');
//SaleTransaction
Route::get('/admin/sales_transaction', 'Admin\SaleTransactionController@viewSaleTransaction');