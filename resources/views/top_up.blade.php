@extends('layouts.navbar')

@section('navbar1')

<div class="container" align="center">
    <div class="col-sm-11">
        <div class="row">
            <div class="col-sm-9" align="left">
                <h4 >TOP UP</h4>
            </div>
            <div class="col-sm-3" align="right">
                <form action="/top_up/search" method="GET">
                    <div class="inner-addon left-addon">
                        <i class="glyphicon glyphicon-user"></i> 
                        <input class="form-control" type="search" name="search" placeholder="Search Game">
                    </div>
                </form>
                <br>
            </div>
        </div>
        <div class="row justify-content-center">
                @foreach($game as $game)
                    <div class="col-sm-3">
    					<a class="card-text" href="top_up/{{ $game->id_game }}">
                        <img class="card-img-top img-responsive rounded" src="{{ asset('uploads/icon/'.$game->image) }}" alt="Game Icon">
    					<div class="card-body">
    						<center><b>{{ $game->game }}</b></center>
    					</div>
                        </a>
    				</div>
                @endforeach
        </div>
    </div>
</div>

@endsection
