@extends('layouts.navbar')

@section('navbar1')

<div class="container-new">
	<div class="bottom">
		<div class="col-md-12">
			<div class="col-lg-8 col-md-8 col-sm-12 left-side">
				<div class="title">
					<p>{{ $select_news->date }}</p>
					<h4>{{ $select_news->title }}</h4>
					<p>author of article: Alvis Elian</p>
				</div>
				<div class="margin-top">
					<img src="{{ asset('uploads/news/'.$select_news->image) }}" class="card-img-top" alt="...">
				</div>
				<div class="margin-top">
					<p>{{ $select_news->description }}</p>	
				</div>
				
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 right-side">
				<div class="col-sm-12 news3">
					<ul class="nav nav-tabs">
					    <li><a class="active" data-toggle="tab" href="#populer"><b>TERPOPULER</b></a></li>
						<li><a data-toggle="tab" href="#baru"><b>TERBARU</b></a></li>
					</ul>

					<div class="tab-content">
					    <div id="populer" class="tab-pane active">
					    	@foreach($news as $news3)
					    	<div class="group">
						    	<div class="col-lg-4 col-md-12 col-sm-6 fl-left">
									<img src="{{ asset('uploads/news/'.$news3->image) }}" class="card-img-top" alt="...">
								</div>
								<div class="col-lg-8 col-md-12 col-sm-6 fl-left">
									<h4 class="card-title"><b>{{ $news3->title }}</b></h4>
									<div class="footer-news">
										<img src="/img/clock.png" alt="...">
										<p>{{ $news3->date }}</p>
										<img src="/img/eye.png" alt="...">
										<p>100 views</p>
									</div>
								</div>
							</div>
							@endforeach
					    </div>
					    <div id="baru" class="tab-pane fade">
					    	@foreach($news->sortBy('date') as $news4)
					    	<div class="group">
						    	<div class="col-lg-4 col-sm-12 fl-left">
									<img src="{{ asset('uploads/news/'.$news4->image) }}" class="card-img-top" alt="...">
								</div>
								<div class="col-lg-8 col-sm-12 fl-left">
									<h4 class="card-title"><b>{{ $news4->title }}</b></h4>
									<div class="footer-news">
										<img src="/img/clock.png" alt="...">
										<p>{{ $news4->date }}</p>
										<img src="/img/eye.png" alt="...">
										<p>100 views</p>
									</div>
								</div>
							</div>
							@endforeach
					    </div>
					</div>
				</div>
				<div class="white-box">___</div>
				<div class="news1">
					<div class="write-text">
						@foreach($news as $news1)
						<div class="col-lg-12 col-sm-12 group">
							<h4 class="card-title"><b>ANDROID BERITA IOS PLATFORM</b></h4>
							<p class="card-text"><b>{{ $news1->title }}</b></p>
							<div class="footer-news">
								<img src="/img/clock.png" alt="...">
								<p>{{ $news1->date }}</p>
							</div>
						</div>
						@endforeach
			    	</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection