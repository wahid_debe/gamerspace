@extends('layouts.navbar')

@section('navbar1')

<div class="container-new">
	<div class="top">
		<div class="col-md-12 padding-0">
			<div class="col-md-9 col-sm-12 news0">
				<div class="write-text">
					<button class="btn btn-warning">PUBG</button>
					<p>PUBG MOBILE Umumkan Kerja Sama dengan The Walking Dead</p>
				</div>
				<img src="img/pubg-news.png">
			</div>
			<div class="col-md-3 col-sm-12 news1">
				<div class="write-text">
					@foreach($news as $news1)
					<a href="news/{{ $news1->id_news }}">
						<div class="col-lg-12 col-sm-12 group">
							<h4 class="card-title"><b>ANDROID BERITA IOS PLATFORM</b></h4>
							<p class="card-text"><b>{{ $news1->title }}</b></p>
							<div class="footer-news">
								<img src="/img/clock.png" alt="...">
								<p>{{ $news1->date }}</p>
							</div>
						</div>
					</a>
					@endforeach
		    	</div>
			</div>
		</div>
	</div>
	<div class="bottom">
		<div class="col-md-12">
			<div class="col-md-8 col-sm-12 news2">
				@foreach($news as $news2)
				<a href="news/{{ $news2->id_news }}">
					<div class="group">
						<div class="col-lg-5 col-md-12 fl-left">
							<img src="{{ asset('uploads/news/'.$news2->image) }}" class="card-img-top" alt="...">
						</div>
						<div class="col-lg-7 col-md-12 fl-left">
							<h4 class="card-title">{{ $news2->title }}</h4>
							<p class="card-text">{{ $news2->description }}</p>
							<div class="footer-news">
								<img src="/img/clock.png" alt="...">
								<p>{{ $news2->date }}</p>
								<img src="/img/eye.png" alt="...">
								<p>100 views</p>
								<img src="/img/black-bubble-speech.png" alt="...">
								<p>5 comments</p>
							</div>
						</div>
					</div>
				</a>
				@endforeach
			</div>
			<div class="col-md-4 col-sm-12 news3">
				<ul class="nav nav-tabs">
				    <li><a class="active" data-toggle="tab" href="#populer"><b>TERPOPULER</b></a></li>
					<li><a data-toggle="tab" href="#baru"><b>TERBARU</b></a></li>
				</ul>

				<div class="tab-content">
				    <div id="populer" class="tab-pane active">
				    	@foreach($news as $news3)
				    	<a href="news/{{ $news3->id_news }}">
					    	<div class="group">
						    	<div class="col-lg-4 col-sm-12 fl-left">
									<img src="{{ asset('uploads/news/'.$news3->image) }}" class="card-img-top" alt="...">
								</div>
								<div class="col-lg-8 col-sm-12 fl-left">
									<h4 class="card-title"><b>{{ $news3->title }}</b></h4>
									<div class="footer-news">
										<img src="/img/clock.png" alt="...">
										<p>{{ $news3->date }}</p>
										<img src="/img/eye.png" alt="...">
										<p>100 views</p>
									</div>
								</div>
							</div>
						</a>
						@endforeach
				    </div>
				    <div id="baru" class="tab-pane fade">
				    	@foreach($news->sortBy('date') as $news4)
				    	<a href="news/{{ $news4->id_news }}">
					    	<div class="group">
						    	<div class="col-lg-4 col-sm-12 fl-left">
									<img src="{{ asset('uploads/news/'.$news4->image) }}" class="card-img-top" alt="...">
								</div>
								<div class="col-lg-8 col-sm-12 fl-left">
									<h4 class="card-title"><b>{{ $news4->title }}</b></h4>
									<div class="footer-news">
										<img src="/img/clock.png" alt="...">
										<p>{{ $news4->date }}</p>
										<img src="/img/eye.png" alt="...">
										<p>100 views</p>
									</div>
								</div>
							</div>
						</a>
						@endforeach
				    </div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection