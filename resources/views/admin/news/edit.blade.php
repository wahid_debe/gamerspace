<!-- modal-edit -->
<div class="modal modal-edit fade" id="edit-modal-{{$news->id_news}}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<h4 class="modal-title" align="center"><b>Edit News</b></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form enctype="multipart/form-data" action="{{ url('/admin/news/save')}}" method="post">
				{{csrf_field()}}
				<div class="modal-body">
					<div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Title</label>
			              </div>
			              <div class="col-sm-9">
			              	<input type="hidden" name="id_news" value="{{$news->id_news}}">
			                <input type="text" class="form-control" name="title" value="{{$news->title}}" placeholder="{{$news->title}}">
			              </div>
			            </div>
			        </div>
			        <div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Image</label>
			              </div>
			              <div class="col-sm-9">
			              	<input type="hidden" name="image1" value="{{$news->image}}">
			                <input type="file" name="image">
			              </div>
			            </div>
			        </div>
			        <div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Description</label>
			              </div>
			              <div class="col-sm-9">
			                <textarea class="textarea" name="description" value="{{$news->description}}" placeholder="{{$news->description}}"
			                style="width: 100%; height: 150px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$news->description}}</textarea>
			              </div>
			            </div>
			        </div>
			        <div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Date</label>
			              </div>
			              <div class="col-sm-9">
			                <input type="date" name="date" value="{{$news->date}}" placeholder="{{$news->date}}">
			              </div>
			            </div>
			        </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>