<!-- modal-delete -->
<div class="modal modal-edit fade" id="delete-modal-{{$news->id_news}}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<h4 class="modal-title" align="center"><b>Delete News</b></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form enctype="multipart/form-data" action="{{ url('/admin/news/delete')}}" method="post">
				{{csrf_field()}}
				<div class="modal-body">
					<input type="hidden" name="id_news" value="{{$news->id_news}}">
					<input type="hidden" name="image" value="{{$news->image}}">
					<h5>Are you sure you want to delete news "{{$news->title}}"</h5>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div>