@extends('layouts.navbar_admin')

@section('content')

<!-- content -->
<div class="card mb-3" style="width: 100%" >
  <div class="card-header">
    <i class="fas fa-table"></i>
    News</div>
  <div class="card-body">
    <a class="btn btn-primary btn-block" style="margin-bottom: 25px" href="/admin/news/add"><i class="fa fa-fw fa-plus"></i><b> Add News</b></a>
    <div class="table-responsive table-striped">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Image</th>
                <th>Title</th>
                <th>Description</th>
                <th>Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Image</th>
                <th>Title</th>
                <th>Description</th>
                <th>Date</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach($news as $news)
            <tr>
                <td width="155px"><img src="{{ asset('uploads/news/'.$news->image) }}" width="150px" height="200px"></td>
                <td>{{ $news->title }}</td>
                <td>{{ $news->description }}</td>
                <td>{{ $news->date }}</td>
                <td>
                    <span data-toggle="modal" data-target="#edit-modal-{{$news->id_news}}"><a href="#" data-placement="top" data-original-title="Edit News" data-toggle="tooltip" class="btn btn-xs btn-success"><i class="fa fa-fw fa-edit"></i> <b class="hover-effect">Edit</b></a></span>
                    @include("admin.news.edit")
                    <span data-toggle="modal" data-target="#delete-modal-{{$news->id_news}}" data-username="{{$news->title}}" data-id="{{$news->id_news}}"><a href="#" data-placement="top" data-original-title="Delete News" data-toggle="tooltip" class="btn btn-xs btn-danger"><i class="fa fa-fw fa-times"></i> <b class="hover-effect">Delete</b></a></span></td>
                    @include("admin.news.delete")
            </tr>
            @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <div class="card-footer small text-muted">Gamers Space</div>
</div>

@endsection