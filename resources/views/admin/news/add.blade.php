@extends('layouts.navbar_admin')

@section('content')

<div class="card mb-3" style="width: 100%">
  <div class="card-header">
    <i class="fas fa-table"></i>
    Add News</div>
  <div class="card-body">
    <form enctype="multipart/form-data" action="{{ url('/admin/news/store')}}" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <div class="form-row">
              <div class="col-sm-3">
                <label>Title</label>
              </div>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="title" placeholder="enter news name" required>
              </div>
            </div>
        </div>
        <div class="form-group">
            <div class="form-row">
              <div class="col-sm-3">
                <label>Image</label>
              </div>
              <div class="col-sm-9">
                <input type="file" name="image" required>
              </div>
            </div>
        </div>
        <div class="form-group">
            <div class="form-row">
              <div class="col-sm-3">
                <label>Description</label>
              </div>
              <div class="col-sm-9">
                <textarea class="textarea" name="description" placeholder="contents of the news"
                style="width: 100%; height: 150px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              </div>
            </div>
        </div>
        <div class="form-group">
            <div class="form-row">
              <div class="col-sm-3">
                <label>Date</label>
              </div>
              <div class="col-sm-9">
                <input type="date" name="date" required>
              </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="/admin/news"><button type="button" class="btn btn-default pull-left">Close</button></a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
  </div>
  <div class="card-footer small text-muted">Gamers Space</div>
</div>
@endsection