@extends('layouts.navbar_admin')

@section('content')

<div class="card mb-3" style="width: 100%">
  <div class="card-header">
    <i class="fas fa-table"></i>
    Add Event</div>
  <div class="card-body">
    <form enctype="multipart/form-data" action="{{ url('/admin/event/store')}}" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <div class="form-row">
              <div class="col-sm-3">
                <label>Event Name</label>
              </div>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="event_name" placeholder="enter event name" required>
              </div>
            </div>
        </div>
        <div class="form-group">
            <div class="form-row">
              <div class="col-sm-3">
                <label>Poster</label>
              </div>
              <div class="col-sm-9">
                <input type="file" name="poster" required>
              </div>
            </div>
        </div>
        <div class="form-group">
            <div class="form-row">
              <div class="col-sm-3">
                <label>Description</label>
              </div>
              <div class="col-sm-9">
                <textarea class="textarea" name="description" placeholder="description of event"
                style="width: 100%; height: 150px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              </div>
            </div>
        </div>
        <div class="form-group">
            <div class="form-row">
              <div class="col-sm-3">
                <label>Date</label>
              </div>
              <div class="col-sm-9">
                <input type="date" name="date" required>
              </div>
            </div>
        </div>
        <div class="form-group">
            <div class="form-row">
              <div class="col-sm-3">
                <label>Contact Person</label>
              </div>
              <div class="col-sm-9">
                <input type="text" name="contact_person" placeholder="enter phone number" required>
              </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="/admin/event"><button type="button" class="btn btn-default pull-left">Close</button></a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
  </div>
  <div class="card-footer small text-muted">Gamers Space</div>
</div>
@endsection