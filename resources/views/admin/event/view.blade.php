@extends('layouts.navbar_admin')

@section('content')

<!-- content -->
<div class="card mb-3" style="width: 100%" >
  <div class="card-header">
    <i class="fas fa-table"></i>
    Event</div>
  <div class="card-body">
    <a class="btn btn-primary btn-block" style="margin-bottom: 25px" href="/admin/event/add"><i class="fa fa-fw fa-plus"></i><b> Add New Event</b></a>
    <div class="table-responsive table-striped">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Poster</th>
                <th>Event Name</th>
                <th>Description</th>
                <th>Date</th>
                <th>Contact Person</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Poster</th>
                <th>Event Name</th>
                <th>Description</th>
                <th>Date</th>
                <th>Contact Person</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach($event as $event)
            <tr>
                <td width="155px"><img src="{{ asset('uploads/event/'.$event->poster) }}" width="150px" height="200px"></td>
                <td>{{ $event->event_name }}</td>
                <td><textarea  placeholder="{{ $event->description }}" style="width: 100%; height: 150px; border: 0; background:rgba(0,0,0,0); resize: none;" disabled></textarea></td>
                <td>{{ $event->date }}</td>
                <td>{{ $event->contact_person }}</td>
                <td>
                    <span data-toggle="modal" data-target="#edit-modal-{{$event->id_event}}"><a href="#" data-placement="top" data-original-title="Ubah Data" data-toggle="tooltip" class="btn btn-xs btn-success"><i class="fa fa-fw fa-edit"></i> <b class="hover-effect">Edit</b></a></span>
                    @include("admin.event.edit")
                    <span data-toggle="modal" data-target="#delete-modal-{{$event->id_event}}" data-username="{{$event->event_name}}" data-id="{{$event->id_event}}"><a href="#" data-placement="top" data-original-title="Delete Event" data-toggle="tooltip" class="btn btn-xs btn-danger"><i class="fa fa-fw fa-times"></i> <b class="hover-effect">Delete</b></a></span></td>
                    @include("admin.event.delete")
            </tr>
            @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <div class="card-footer small text-muted">Gamers Space</div>
</div>

@endsection