@extends('layouts.navbar_admin')

@section('content')

<div class="card mb-3" style="width: 100%">
  <div class="card-header">
    <i class="fas fa-table"></i>
    Edit Game</div>
  <div class="card-body">
    <a class="btn btn-primary col-sm-12" style="margin-bottom: 10px;" href="/admin/topup/addGame"><i class="fa fa-fw fa-plus"></i><b> Add New Game</b></a>
    <a class="btn btn-primary col-sm-12" style="margin-bottom: 10px;" href="/admin/topup"><b> Back</b></a>
	<div class="table-responsive table-striped">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Image</th>
                <th>Game Name</th>
                <th>Currency</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Image</th>
                <th>Game Name</th>
                <th>Currency</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach($game as $game)
            <tr>
                <td width="155px"><img src="{{ asset('uploads/icon/'.$game->image) }}" width="150px" height="150px"></td>
                <td>{{ $game->game }}</td>
                <td>{{ $game->currency }}</td>
                <td>
                    <span data-toggle="modal" data-target="#edit-modal-{{$game->id_game}}"><a href="#" data-placement="top" data-original-title="Edit game" data-toggle="tooltip" class="btn btn-xs btn-success"><i class="fa fa-fw fa-edit"></i> <b class="hover-effect">Edit</b></a></span>
                    @include("admin.topup.editGame")
                    <span data-toggle="modal" data-target="#delete-modal-{{$game->id_game}}" data-username="{{$game->game}}" data-id="{{$game->id_game}}"><a href="#" data-placement="top" data-original-title="Delete game" data-toggle="tooltip" class="btn btn-xs btn-danger"><i class="fa fa-fw fa-times"></i> <b class="hover-effect">Delete</b></a></span></td>
                    @include("admin.topup.deleteGame")

            </tr>
            @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <div class="card-footer small text-muted">Gamers Space</div>
</div>
@endsection