<!-- modal-delete -->
<div class="modal modal-edit fade" id="delete-modal-{{$game->id_game}}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<h4 class="modal-title" align="center"><b>Delete game</b></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form enctype="multipart/form-data" action="{{ url('/admin/topup/deleteGame')}}" method="post">
				{{csrf_field()}}
				<div class="modal-body">
					<input type="hidden" name="id_game" value="{{$game->id_game}}">
					<input type="hidden" name="image" value="{{$game->image}}">
					<h5>Are you sure you want to delete game "{{$game->game}}"</h5>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div>