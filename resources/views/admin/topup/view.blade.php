@extends('layouts.navbar_admin')

@section('content')

<!-- content -->
<div class="card mb-3" style="width: 100%" >
  <div class="card-header">
    <i class="fas fa-table"></i>
    Top Up</div>
  <div class="card-body">
    <a class="btn btn-primary col-sm-12" style="margin-bottom: 10px;" href="/admin/topup/addGame"><i class="fa fa-fw fa-plus"></i><b> Add New Game</b></a>
    <a class="btn btn-success col-sm-12" style="margin-bottom: 10px;" href="/admin/topup/game"><i class="fa fa-fw fa-edit"></i><b> Edit Game</b></a>
    <a class="btn btn-primary col-sm-12" style="margin-bottom: 25px" href="/admin/topup/addPrice"><i class="fa fa-fw fa-plus"></i><b> Add New Price</b></a>
    <div class="table-responsive table-striped">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Image</th>
                <th>Games Name</th>
                <th>Balance</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Image</th>
                <th>Games Name</th>
                <th>Balance</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach($topup->sortBy('id_game') as $topup)
            <tr>
                <td width="155px"><img src="{{ asset('uploads/icon/'.$topup->image) }}" width="150px" height="150px"></td>
                <td>{{ $topup->game }}</td>
                <td>{{ $topup->balance }} {{ $topup->currency }}</td>
                <td align="right">Rp. {{ $topup->price }},00</td>
                <td>
                    <span data-toggle="modal" data-target="#edit-modal-{{$topup->id_topup}}"><a href="#" data-placement="top" data-original-title="Edit topup" data-toggle="tooltip" class="btn btn-xs btn-success"><i class="fa fa-fw fa-edit"></i> <b class="hover-effect">Edit</b></a></span>
                    @include("admin.topup.editPrice")
                    <span data-toggle="modal" data-target="#delete-modal-{{$topup->id_topup}}" data-username="{{$topup->game}}" data-id="{{$topup->id_topup}}"><a href="#" data-placement="top" data-original-title="Delete topup" data-toggle="tooltip" class="btn btn-xs btn-danger"><i class="fa fa-fw fa-times"></i> <b class="hover-effect">Delete</b></a></span></td>
                    @include("admin.topup.deletePrice")
            </tr>
            @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <div class="card-footer small text-muted">Gamers Space</div>
</div>

@endsection