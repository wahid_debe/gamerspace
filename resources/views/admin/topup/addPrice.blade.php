@extends('layouts.navbar_admin')

@section('content')

<div class="card mb-3" style="width: 100%">
  <div class="card-header">
    <i class="fas fa-table"></i>
    Add Top Up Price</div>
  <div class="card-body">
	<form enctype="multipart/form-data" action="{{ url('/admin/topup/storePrice')}}" method="post">
		{{csrf_field()}}
		<div class="modal-body">
			<div class="form-group">
	            <div class="form-row">
	              <div class="col-sm-3">
	                <label>Game Name</label>
	              </div>
	              <div class="col-sm-9">
	                <select name="id_game" class="form-control select2" style="width: 100%;" data-error="Maaf, pertemuan tidak boleh kosong." required>
                        <option disabled selected value>-</option>
                        @foreach($game->sortBy('id_game') as $game)
                        <option value="{{$game->id_game}}">{{$game->game}}@endforeach</option>
                    </select>
	              </div>
	            </div>
	        </div>
	        <div class="form-group">
	            <div class="form-row">
	              <div class="col-sm-3">
	                <label>Balance</label>
	              </div>
	              <div class="col-sm-9">
	                <input type="number" class="form-control" name="balance" required>
	              </div>
	            </div>
	        </div>
	        <div class="form-group">
	            <div class="form-row">
	              <div class="col-sm-3">
	                <label>Price</label>
	              </div>
	              <div class="col-sm-9">
	                <input type="number" class="form-control" name="price" required>
	              </div>
	            </div>
	        </div>
		</div>
        <div class="modal-footer">
            <a href="/admin/topup"><button type="button" class="btn btn-default pull-left">Close</button></a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
	</form>
  </div>
  <div class="card-footer small text-muted">Gamers Space</div>
</div>
@endsection