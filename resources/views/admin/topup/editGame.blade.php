<!-- modal-edit -->
<div class="modal modal-edit fade" id="edit-modal-{{$game->id_game}}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<h4 class="modal-game" align="center"><b>Edit Game</b></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form enctype="multipart/form-data" action="{{ url('/admin/topup/saveGame')}}" method="post">
				{{csrf_field()}}
				<div class="modal-body">
					<div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Game Name</label>
			              </div>
			              <div class="col-sm-9">
			              	<input type="hidden" name="id_game" value="{{$game->id_game}}">
			                <input type="text" class="form-control" name="game" value="{{$game->game}}" placeholder="{{$game->game}}">
			              </div>
			            </div>
			        </div>
			        <div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Image</label>
			              </div>
			              <div class="col-sm-9">
			              	<input type="hidden" name="image1" value="{{$game->image}}">
			                <input type="file" name="image">
			              </div>
			            </div>
			        </div>
					<div class="form-group">
			            <div class="form-row">
			              <div class="col-sm-3">
			                <label>Currency</label>
			              </div>
			              <div class="col-sm-9">
			                <input type="text" class="form-control" name="currency" value="{{$game->currency}}" placeholder="{{$game->currency}}">
			              </div>
			            </div>
			        </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>