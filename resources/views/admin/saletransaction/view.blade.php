@extends('layouts.navbar_admin')

@section('content')

<div class="card mb-3" style="width: 100%">
  <div class="card-header">
    <i class="fas fa-table"></i>
    Sales Transaction</div>
  <div class="card-body">
    <div class="table-responsive table-striped">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Transaction ID</th>
                <th>Price</th>
                <th>Create Date</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Transaction ID</th>
                <th>Price</th>
                <th>Create Date</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach($sale as $sale)
            <tr>
                <td>{{ $sale->id_transaction }}</td>
                <td>{{ $sale->price }}</td>
                <td>{{ $sale->created_at }}</td>
            </tr>
            @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <div class="card-footer small text-muted">Gamers Space</div>
</div>
@endsection