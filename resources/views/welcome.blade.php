@extends('layouts.navbar')

@section('navbar1')

<div class="content">
    <div class="container">
      
      <!-- iklan -->
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                      <div class="carousel-item active" data-interval="10000">
                        <img src="/img/Rectangle 3.2.png" class="d-block w-100" alt="...">
                      </div>
                      <div class="carousel-item" data-interval="2000">
                        <img src="/img/Rectangle 3.2.png"class="d-block w-100" alt="...">
                      </div>
                      <div class="carousel-item">
                        <img src="/img/Rectangle 3.2.png" class="d-block w-100" alt="...">
                      </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
            </div>

            <!-- top up-->
            <h2 class="text-center d-lg-flex justify-content-lg-center" style="margin-top: 80px;font-size: 36px;font-family: 'Playfair Display', serif;margin-bottom:20px;"><strong>Top Up</strong></h2>
                      <div class="card-deck">
                        @foreach($games as $games)
                        <div class="card" style="width: 18rem;">
                                <img src="{{ asset('uploads/icon/'.$games->image) }}" class="card-img-top" alt="...">
                                <div class="card-body">
                                        <p class="text-center card-text sub-text-color" style="font-family: 'Nunito Sans', sans-serif;font-size: 18px;color: rgb(0,0,0);"><strong>{{ $games->game }}</strong></p>
                                </div>
                        </div>
                        @endforeach
                      </div>


          <center>
          <a class="btn btn-warning" style="background:#F6C400;margin-bottom: 40px; " href="">{{ __('SEE MORE') }}</a>




        <!-- merchandise -->
        <center>
          <h2 class="text-center d-lg-flex justify-content-lg-center" style="margin-top: 80px;margin-bottom: 0px;font-family: 'Playfair Display', serif;margin-bottom:20px;"><strong>Dapatkan Merchandise Team Favorite Kalian</strong></h2>
        <div class="card-deck">
            <div class="card" style="width: 18rem;">
                    <img src="" class="card-img-top" alt="Product">
                    <div class="card-body">
                              <p class="text-left card-text sub-text-color" style="font-size: 18px;font-family: 'Nunito Sans', sans-serif;color: rgb(0,0,0);margin-bottom: 15px;"><strong>Product</strong></p>
                              <p class="text-left card-text sub-text-color" style="font-size: 18px;font-family: 'Nunito Sans', sans-serif;color: rgb(0,0,0);">Price</p>
                    </div>
            </div>
            <div class="card" style="width: 18rem;">
                    <img src="" class="card-img-top" alt="Product">
                    <div class="card-body">
                      <p class="text-left card-text sub-text-color" style="font-size: 18px;font-family: 'Nunito Sans', sans-serif;color: rgb(0,0,0);margin-bottom: 15px;"><strong>Product</strong></p>
                      <p class="text-left card-text sub-text-color" style="font-size: 18px;font-family: 'Nunito Sans', sans-serif;color: rgb(0,0,0);">Price</p>
                    </div>
            </div>
            <div class="card" style="width: 18rem;">
                    <img src="" class="card-img-top" alt="Product">
                    <div class="card-body">
                      <p class="text-left card-text sub-text-color" style="font-size: 18px;font-family: 'Nunito Sans', sans-serif;color: rgb(0,0,0);margin-bottom: 15px;"><strong>Product</strong></p>
                      <p class="text-left card-text sub-text-color" style="font-size: 18px;font-family: 'Nunito Sans', sans-serif;color: rgb(0,0,0);">Price</p>
                    </div>
            </div>
          </div>
          <center>
          <a class="btn btn-warning" style="background:#F6C400;margin-bottom: 40px;  " href="">{{ __('SEE MORE') }}</a>


    </div>


    <!-- Benefits -->
    <section class="features-icons text-center" style="background:#0064D2">
        <center>
            <h2 style="color:#ffffff">Our Benefits</h2>
      <div class="row">
        <div class="col-lg-3">
          <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
            <div class="features-icons-icon d-flex">
                <i class="m-auto"><img src="/img/id-card.png" alt=""></i>
            </div>
            <h6 style="font-size:20px;color:#ffffff">Membership</h6>
            <p class="lead mb-0" style="font-size:14px;color:#ffffff">Daftar sebagai member kemudian dapatkan berbagai keuntungan sebagai Gamers Space</p>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
            <div class="features-icons-icon d-flex">
                <i class="m-auto"><img src="/img/social-care.png" alt=""></i>
            </div>
            <h6 style="font-size:20px;color:#ffffff">Komunitas Besar</h6>
            <p class="lead mb-0" style="font-size:14px;color:#ffffff">Temukan komunitas dari seluruh indonesia dan saling terhubung satu sama lain</p>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="features-icons-item mx-auto mb-0 mb-lg-3">
            <div class="features-icons-icon d-flex">
                <i class="m-auto"><img src="/img/trophy.png" alt=""></i>
            </div>
            <h6 style="font-size:20px;color:#ffffff">Event Menarik</h6>
            <p class="lead mb-0" style="font-size:14px;color:#ffffff">Temukan event yang kamu suka lalu dapat berpartisipasi dalam event dengan total hadiah yang besar</p>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="features-icons-item mx-auto mb-0 mb-lg-3">
            <div class="features-icons-icon d-flex">
                <i class="m-auto"><img src="/img/tag.png" alt=""></i>
            </div>
            <h6 style="font-size:20px;color:#ffffff">Diskon Setiap Saat</h6>
            <p class="lead mb-0" style="font-size:14px;color:#ffffff">Dapatkan diskon setiap anda ingin Topup dan membeli item dari game yang anda sukai</p>
          </div>
        </div>
      </div>
  </section>

  <div class="container">

        <!-- merchandise -->
        <center>
          <h2 class="text-center d-lg-flex justify-content-lg-center" style="margin-top: 80px;margin-bottom: 0px;font-family: 'Playfair Display', serif;margin-bottom:20px;"><strong>Top News About E-Sport</strong></h2>
        <div class="card-deck">
            @foreach($news as $news) 
            <div class="card" style="width:18rem;">
                    <img src="{{ asset('uploads/news/'.$news->image) }}" class="card-img-top" alt="News">
                    <div class="card-body">
                      <p class="text-center card-text sub-text-color" style="color: #fe7171;font-family: 'Nunito Sans', sans-serif;font-size: 14px;margin-bottom: 10px;"><strong>GAME</strong></p>
                      <p class="text-center card-text sub-text-color" style="color: rgb(0,0,0);font-family: 'Nunito Sans', sans-serif;font-size: 14px;margin-bottom: 15px;"><strong>{{ $news->title }}</strong></p>
                      <p class="text-center card-text sub-text-color" style="color: rgb(0,0,0);font-family: 'Nunito Sans', sans-serif;font-size: 14px;">{{ $news->date }}</p>
                    </div>
            </div>
            @endforeach
            <!-- <div class="card" style="width: 18rem;">
                    <img src="" class="card-img-top" alt="News">
                    <div class="card-body">
                      <p class="text-center card-text sub-text-color" style="color: #fe7171;font-family: 'Nunito Sans', sans-serif;font-size: 14px;margin-bottom: 10px;"><strong>GAME</strong></p>
                      <p class="text-center card-text sub-text-color" style="color: rgb(0,0,0);font-family: 'Nunito Sans', sans-serif;font-size: 14px;margin-bottom: 15px;"><strong>TITLE</strong></p>
                      <p class="text-center card-text sub-text-color" style="color: rgb(0,0,0);font-family: 'Nunito Sans', sans-serif;font-size: 14px;">Date</p>
                    </div>
            </div>
            <div class="card" style="width: 18rem;">
                    <img src="" class="card-img-top" alt="News">
                    <div class="card-body">
                      <p class="text-center card-text sub-text-color" style="color: #fe7171;font-family: 'Nunito Sans', sans-serif;font-size: 14px;margin-bottom: 10px;"><strong>GAME</strong></p>
                      <p class="text-center card-text sub-text-color" style="color: rgb(0,0,0);font-family: 'Nunito Sans', sans-serif;font-size: 14px;margin-bottom: 15px;"><strong>TITLE</strong></p>
                      <p class="text-center card-text sub-text-color" style="color: rgb(0,0,0);font-family: 'Nunito Sans', sans-serif;font-size: 14px;">Date</p>
                    </div>
            </div> -->
          </div>
  </div>

    <section id="features" style="background-color: #0064d2;margin-top: 44px;">
        <div id="container" class="container-fluid">
            <div id="row" class="row">
                <div class="col-lg-8 offset-lg-4">
                    <div class="ection-header wow fadeIn" style="visibility: visible; animation-duration: 1s; animation-name: fadeIn;" data-wow-duration="1s">
                        <h3 class="text-left section-title" style="color: #ffffff;font-size: 96px;margin-bottom: 0px;">“</h3><span class="section-divider"></span></div>
                </div>
                <div class="col-lg-4 col-md-5 features-img"><img src="img/image.png"></div>
                <div class="col-lg-8 col-md-7">
                    <div class="row">
                        <div class="col-lg-10 col-md-10 box wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                            <div></div>
                            <h4 class="text-justify title" style="color: rgb(255,255,255);">Mengikuti beberapa komunitas di Gamers Space membuat saya banyak teman mabar dan membuat saya bisa meningkatkan gameplay saya lebih baik lagi&nbsp;<br></h4>
                            <div class="row"><img class="col-md-5 col-lg-4" src="img/Rectangle%203.2.png" style="height: 8px;width: 166px;">
                                <div class="col-lg-5 col-md-10" style="font-size: 24px;color: rgb(255,255,255);font-family: 'Nunito Sans', sans-serif;">
                                    <p class="text-left description col-md-12 col-lg-12" style="font-size: 24px;margin-bottom: 15px;"><strong>Nizar Lugatio</strong><br></p>
                                    <p class="text-left description col-md-12 col-lg-12" style="font-size: 18px;color: rgb(255,255,255);font-family: 'Nunito Sans', sans-serif;">BTR E-Sport Player<br></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="features" style="background-color: #ffffff;margin-top: 100px;">
        <div id="container" class="container-fluid">
            <div id="row" class="row">
                <div class="col-lg-8 offset-lg-4">
                    <div class="ection-header wow fadeIn" style="visibility: visible; animation-duration: 1s; animation-name: fadeIn;" data-wow-duration="1s"><span class="section-divider"></span></div>
                </div>
                <div class="col-lg-5 col-md-6 features-img"><img src="img/undraw_game_day_ucx9.png"></div>
                <div class="col-lg-5 col-md-5" style="visibility: visible; animation-name: fadeInRight;">
                    <div class="row">
                        <div class="col-lg-10 col-md-10 box wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                            <h4 class="text-left title" style="color: rgb(0,0,0);font-family: 'Playfair Display', serif;font-size: 36px;"><strong>Datang dan Dukung Team </strong><br><strong>Kesayangan Kalian</strong></h4>
                            <p class="text-left sub-text-color" style="color: rgb(0,0,0);font-family: 'Nunito Sans', sans-serif;font-size: 18px;">Saksikan event game yang menarik dan kemudian dukung team kesayangan kalian memenangkan event tersebut. Raih jutaan hadiah kejutan setiap event.<br></p>
                        </div>
                    </div><button class="btn btn-warning" type="button">FIND EVENT</button></div>
            </div>
        </div>
    </section>

</div>


@endsection